/* Copyright 2017 newbrain */

#ifndef __CECbuffer_H
#define __CECbuffer_H

#define CEC_MAX_PAYLOAD         16
#define CEC_MAX_BUFFERS         8

extern volatile uint16_t RxLen; /* Current Frame Length     */
extern volatile uint8_t *RxBuf; /* Pointer to a frame in the buffer */

/* Actual receive buffer MUST used by HAL */
extern uint8_t  CecBuf[CEC_MAX_PAYLOAD+8];

void FlushBuffers( void );
void EnqueueBuffer( uint16_t size );
void DequeueBuffer( void );

#endif
