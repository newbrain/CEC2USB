/* Copyright 2017 newbrain */

#ifndef __CECmessages_H
#define __CECmessages_H


static const uint8_t CECtoHID[0x80] = {
// Translation from CEC buttons to HID keycodes.
// If autorepeat is desired, bit 7 must be 1. Lots of holes.
//    00      01      02      03      04      05      06      07
// Select     Up     Down   Left    Right     RU      RD      LU
// Enter      Up     Down   Left    Right     --      --      --
    0x28,   0xD2,   0xD1,   0xD0,   0xCF,   0x00,   0x00,   0x00,
//    08      09      0A      0B      0C      0D      0E      0F
//    --    Home   Options  List      --    Exit      --      --
//    --     Esc      C      Tab      --  Backspace   --      --
    0x00,   0x29,   0x06,   0x2B,   0x00,   0x2A,   0x00,   0x00,
//    10      11      12      13      14      15      16      17
    0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,
//    18      19      1A      1B      1C      1D      1E      1F
    0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,
//    20      21      22      23      24      25      26      27
    0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,
//    28      29      2A      2B      2C      2D      2E      2F
    0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,
//    30      31      32      33      34      35      36      37
    0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,
//    38      39      3A      3B      3C      3D      3E      3F
    0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,
//    40      41      42      43      44      45      46      47
//    --      --      --      --    Play    Stop    Pause     --
//    --      --      --      --      P       X     Space     --
    0x00,   0x00,   0x00,   0x00,   0x13,   0x1B,   0x2C,   0x00,
//    48      49      4A      4B      4C      4D      4E      4F
//   Rew    FFwd      --   Forward Backward   --      --      --
//     R       F      --      .       ,       --      --      --
    0x15,   0x09,   0x00,   0xB7,   0xB6,   0x00,   0x00,   0x00,
//    50      51      52      53      54      55      56      57
    0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,
//    58      59      5A      5B      5C      5D      5E      5F
    0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,
//    60      61      62      63      64      65      66      67
    0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,
//    68      69      6A      6B      6C      6D      6E      6F
    0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,
//    70      71      72      73      74      75      76      77
//    --    Blue     Red    Green   Yellow    --      --      --
//    --    PgUp    PgDn    Info    Codec     --      --      --
    0x00,   0xCB,   0xCE,   0x0C,   0x12,   0x00,   0x00,   0x00,
//    78      79      7A      7B      7C      7D      7E      7F
    0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00,   0x00
};


// Constant messages

// Prepared buffer with OSD name of the device
static const uint8_t OSDname[9]   = {
    HDMI_CEC_OPCODE_SET_OSD_NAME,'n','e','w','b','r','a','i','n'
};

// Prepared buffer with CEC version
static const uint8_t CECversion[2]   = {
    HDMI_CEC_OPCODE_CEC_VERSION,HDMI_CEC_VERSION
};

// Prepared buffer with Device Vendor Id
// That would be expensive, just use my name...
static const uint8_t VendorId[4]   = {
    HDMI_CEC_OPCODE_DEVICE_VENDOR_ID, 0xFE, 0xDE, 0x01
};

// Prepared buffer with Power Status
static const uint8_t PowerStatus[2]   = {
    HDMI_CEC_OPCODE_REPORT_POWER_STATUS, 0x00
};

// Variable messages

#define PHYS_HI PHYS_DEFAULT
#define PHYS_LO 0x00
// Prepared buffer with REPORT PHYSICALADDRESSMESSAGE
static uint8_t PhysicalAddress[4]= {
    HDMI_CEC_OPCODE_REPORT_PHYSICAL_ADDRESS, PHYS_HI, PHYS_LO, HDMI_CEC_PLAYBACK
};

// Prepared buffer with Active Source
static uint8_t ActiveSource[4] = {
  HDMI_CEC_OPCODE_ACTIVE_SOURCE,
  PHYS_HI,
  PHYS_LO,
  0 // just for alignment
};

// Prepared buffer with Inactive Source
static uint8_t InactiveSource[4] = {
  HDMI_CEC_OPCODE_INACTIVE_SOURCE,
  PHYS_HI,
  PHYS_LO,
  0 // just for alignment
};

// Prepared buffer with Feature Abort
static uint8_t FeatureAbort[4]   = {
    HDMI_CEC_OPCODE_FEATURE_ABORT, 0x00, HDMI_CEC_UNRECOGNIZED_OPCODE, 0 // just for alignment
};

#endif
