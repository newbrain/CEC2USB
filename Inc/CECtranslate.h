/* Copyright 2017 newbrain */

#ifndef __CECtranslate_H
#define __CECtranslate_H

// Logical address allocation
void CecInitialExchange(uint8_t physHi);

// Handle messaging and translation
void Translate(void);


// Debugging aids
extern int debug;
#define toHex(x) (((x) > 9) ? ((x)+'A'-10) : ((x)+'0'))

#endif
