/* Copyright 2017 newbrain */

# pragma once

#include "stm32f0xx_hal.h"

#define CEC_GPIO    GPIOA
#define CEC_PIN     GPIO_PIN_5

#define KEY_GPIO    GPIOB
#define KEY_PIN     GPIO_PIN_0
#define KEY_IRQ     EXTI0_1_IRQn

#define LED_GPIO    GPIOB
#define LED_PIN     GPIO_PIN_3

#define I2C_GPIO    GPIOA
#define I2C_SCL     GPIO_PIN_9
#define I2C_SDA     GPIO_PIN_10

#define USB_GPIO    GPIOA
#define USB_DP      GPIO_PIN_12
#define USB_DM      GPIO_PIN_11

#define UART_GPIO   GPIOA
#define UART_TX     GPIO_PIN_2
#define UART_RX     GPIO_PIN_15



