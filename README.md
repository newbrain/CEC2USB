CEC2USB
---

The code needs the latest (1.7.0) STM32Cube for F0 MCUs, it uses the HAL and the USB device core library.
It's up to you to create a makefile/project for your favourite environment.

HW
--

I used an STM32F0K6 Nucleo board as platform, a 27kOhm resistor and a 2N7000 are needed to isolate the bus from the MCU, as described in many ST ANs and RMs.

The physical address is preprogrammed, and a pushbutton can be used to change it.

It has been tested ony with some Sony TVs.
