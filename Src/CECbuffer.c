/* Copyright(C) 2017 newbrain */


/* Includes ------------------------------------------------------------------*/
#include "stm32f0xx_hal.h"
#include "hdmi_cec.h"
#include "CECbuffer.h"

/* Circular buffer housekeeping variables
 * volatile as the they are written in ISR
 * and read outside
 */
static volatile uint16_t RxWrite  = 0; /* Current write position   */
static volatile uint16_t RxRead   = 0; /* Current read position    */
static volatile uint16_t Frames   = 0; /* Current Frames in buffer */

// Public variables
volatile uint16_t RxLen    = 0; /* Current Frame Length     */
volatile uint8_t *RxBuf = NULL; /* Pointer to a frame in the buffer */

/* Received data buffer. Max size = 16 bytes
 * header + opcode followed by up to 14 operands
 * by 8 times for circular buffer */
__ALIGN_BEGIN static uint8_t  Tab_Rx[CEC_MAX_BUFFERS][CEC_MAX_PAYLOAD] __ALIGN_END ;

/* Number of received bytes including the header.
 * when a ping message has been received (header
 * only), RxBytes[?] = 1 */
static uint16_t RxBytes[CEC_MAX_BUFFERS];

/* Actual receive buffer used by HAL, sometimes frames longer than expected arrive. */
__ALIGN_BEGIN uint8_t  CecBuf[CEC_MAX_PAYLOAD+8] __ALIGN_END ;


void FlushBuffers( void )
{
 RxWrite = 0; /* Current write position   */
 RxRead  = 0; /* Current read position    */
 Frames  = 0; /* Current Frames in buffer */

  // Public variables
 RxLen   = 0; /* Current Frame Length     */
 RxBuf = NULL; /* Pointer to a frame in the buffer */
}

/* Called to insert a new frame in the buffer */
void EnqueueBuffer( uint16_t size )
{
  if(Frames == CEC_MAX_BUFFERS) return; /* No space left. Frame will be lost */

  uint32_t *src = (uint32_t *)CecBuf;
  uint32_t *dst = (uint32_t *)Tab_Rx[RxWrite];
  /* Copy word by word, faster?  */
  *dst++ = *src++;
  *dst++ = *src++;
  *dst++ = *src++;
  *dst++ = *src++;
  /* Save also rx length */
  RxBytes[RxWrite] = size+1;

  /* Another frame in buffer */
  Frames++;

  /* If we did not have a frame, update
   * RxBuf and (just to be safe) RxRead */
  if(Frames == 1)
  {
    RxRead = RxWrite;
    RxBuf = Tab_Rx[RxRead];
    RxLen = RxBytes[RxRead];
  }

  /* And a new buffer to write to */
  RxWrite++;
  /* If we hit end of the buffer, restart from 0 */
  if( RxWrite == CEC_MAX_BUFFERS) RxWrite = 0;
}

/* Called to signal the end of processing for
 * the frame pointed by RxBuf.
 * If nothing left to process, RxBuf = null
 */
void DequeueBuffer()
{
  __disable_irq();
  /* If we hit end of the buffer, restart from 0 */
  if( ++RxRead == CEC_MAX_BUFFERS) RxRead = 0;

  /* One frame less in buffer */
  Frames--;
  if(Frames == 0) {
    RxBuf = NULL;   /* If nothing left, no RxBuf */
    RxLen = 0;
  } else {
    RxBuf = Tab_Rx[RxRead];
    RxLen = RxBytes[RxRead]; /* Include header */
  }
  __enable_irq();
}

