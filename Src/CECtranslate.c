/* Copyright 2017 newbrain */

/* Includes */
#include <stdbool.h>
#include "stm32f0xx_hal.h"
#include "hdmi_cec.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"
#include "usbd_kb_hid.h"
#include "CECtranslate.h"
#include "CECbuffer.h"
#include "CECmessages.h"
#include "PortDefs.h"

// debug flag
int debug = 0;


// Keep the last key received (0xFF means a release has been received)
#define NO_KEY 0xFF
static uint8_t LastKey = NO_KEY;

// Pulse LED 50ms to indicate communications. Retriggerable.
#define LED_TO (uint32_t)50
static uint32_t ledTick = 0;
static uint16_t ledOn   = 0;
static void pulseLED( void );


static uint8_t cecAdr[17]   = "CEC Addr 0xn(x)\r\n";


// My physical address
static uint8_t physicalAddress;

// Active Source Status
static bool activeSource = false;

void CecInitialExchange( uint8_t physAddr )
{
  /* Get an address, try forever every 1s until ok
   *         << 44
   */
  while( HAL_OK != HDMI_CEC_LogicalAddressAllocation() )
  {
    HAL_Delay( 500 );
    HAL_GPIO_TogglePin( LED_GPIO, LED_PIN );
    HAL_Delay( 500 );
    HAL_GPIO_TogglePin( LED_GPIO, LED_PIN );
  }

  //Not really needed
  FlushBuffers();

  // Message with physical and logical addresses
  cecAdr[11] = toHex( logicalAddress );
  cecAdr[13] = toHex(physAddr >> 4);
  HAL_UART_Transmit( &huart2, cecAdr, 17, 500 );

  // Store physical address in the messages
  PhysicalAddress[1] = physAddr;
  ActiveSource[1]    = physAddr;
  InactiveSource[1]  = physAddr;
  physicalAddress    = physAddr;
}

static uint8_t cecFrame[42] = "CEC[ ]                 ";

uint16_t formatCEC(uint8_t *b, uint32_t l)
{
  uint8_t *p = cecFrame + 4;
  *p++ = toHex(l);
  p += 2;
  for (uint16_t i = 0; i < l; i++)
  {
    uint8_t c  = b[i];
    *p++ = toHex(c >> 4);
    *p++ = toHex(c & 0xF);
  }
  *p++ = '\r';
  *p++ = '\n';

  return p - cecFrame;
}



int16_t AnswerCEC( void )
{
  uint16_t key = -1;
  uint16_t Answer = 1;

  const uint8_t *TxBuf;
  uint32_t TxSize;
  uint8_t  TxDest;

  // Destination and opcode of _RECEIVED_ message
  uint8_t dst = RxBuf[0] & HDMI_CEC_BROADCAST;
  uint8_t opc = RxBuf[1];

  /* Was it a ping, or directed to another device and not a broadcast? */
  if( (RxLen == 1)  || ((dst != HDMI_CEC_BROADCAST) && (dst != logicalAddress)) ) return key;

  TxDest = RxBuf[0] >> 4;

  switch( opc ) {

  case HDMI_CEC_OPCODE_GIVE_PHYSICAL_ADDRESS:
    TxBuf  = PhysicalAddress;
    TxSize = 4;
    TxDest = HDMI_CEC_BROADCAST;
    break;

  case HDMI_CEC_OPCODE_GIVE_OSD_NAME:
    TxBuf  = OSDname;
    TxSize = 9;
    break;

  case HDMI_CEC_OPCODE_GET_CEC_VERSION:
    TxBuf  = CECversion;
    TxSize = 2;
    break;

  case HDMI_CEC_OPCODE_GIVE_DEVICE_VENDOR_ID:
    TxBuf  = VendorId;
    TxSize = 4;
    TxDest = HDMI_CEC_BROADCAST;

    break;

  case HDMI_CEC_OPCODE_ROUTING_CHANGE:
    activeSource = (RxBuf[4] == physicalAddress);
    if (activeSource)
    { // Activate if routing changed to us
      TxBuf = ActiveSource;
    }
    else
    { // Inactivate if it changed to something else.
      TxBuf = InactiveSource;
    }
    TxSize = 3;
    TxDest = HDMI_CEC_BROADCAST;
    break;

  case HDMI_CEC_OPCODE_REQUEST_ACTIVE_SOURCE:
    // Answer if we are the active source
    if (activeSource)
    {
      TxBuf = ActiveSource;
      TxSize = 3;
      TxDest = HDMI_CEC_BROADCAST;
    }
    else
    {
      Answer = 0;
    }
    break;
    
  case HDMI_CEC_OPCODE_SET_STREAM_PATH:
    // Answer if we are the selected stream, and become active source
    if (
      (dst == logicalAddress) 
      ||((dst == HDMI_CEC_BROADCAST) && (RxBuf[2] = physicalAddress))
      )
    {
      activeSource = true;
      TxBuf = ActiveSource;
      TxSize = 3;
      TxDest = HDMI_CEC_BROADCAST;
    }
    else
    {
      Answer = 0;
    }
    break;
    
  case HDMI_CEC_OPCODE_GIVE_DEVICE_POWER_STATUS:
    TxBuf  = PowerStatus;
    TxSize = 2;
    break;

  case HDMI_CEC_OPCODE_USER_CONTROL_PRESSED:
    key    = RxBuf[2];
    Answer = 0;
    break;

  case HDMI_CEC_OPCODE_USER_CONTROL_RELEASED:
    LastKey = NO_KEY;
    Answer  = 0;
    break;

  default: // Unknown message, answer with feature abort
    // No Feature Abort on broadcast
    if( dst == HDMI_CEC_BROADCAST ) Answer = 0;
    FeatureAbort[1] = opc;
    TxBuf           = FeatureAbort;
    TxSize          = 3;
  }

  if( Answer )
  {
    pulseLED();
    HAL_CEC_Transmit_IT( &hcec, logicalAddress, TxDest, (uint8_t*)TxBuf, TxSize );
    if (debug)
    {
      uint16_t l = formatCEC((uint8_t *)TxBuf, TxSize);
      HAL_UART_Transmit(&huart2, cecFrame, l, 250);
    }
  }

  return key;
}

static uint8_t HIDreport[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };

typedef enum
{
  REPORT_IDLE,
  REPORT_PRESS,
  REPORT_RELEASE
} ReportStatus;

static volatile ReportStatus reportAction = REPORT_IDLE;

void HandleKeys( int16_t keyin )
{
  uint8_t key;
  uint8_t rpt;
  static uint32_t keyTick;
  uint32_t newTick = HAL_GetTick();

  if( keyin > 0x76 || keyin < 0x00 ) return; // Out of range
  key = CECtoHID[keyin] & 0x7F; // HID key value
  rpt = CECtoHID[keyin] > 0x7F; // Auto-repeat key flag
  if(
      (key == LastKey) &&                 // Received a repeating key without an intervening release
      !rpt &&                             // Repeat is not desired for these keys
      ((newTick - keyTick) < 1000) )      // But after 1000ms we might have missed the release...
  {
    return;
  }
  // Send Key and remember code and time
  LastKey = key;
  keyTick = newTick;

  //Key pressed
  HIDreport[2] = key;
  reportAction = REPORT_PRESS;

  pulseLED();
}


void Translate()
{
  // Handle key report mini statemachine and new CEC messages
  // wait for a (synthetic) key release before sending a new one
  if ((reportAction == REPORT_IDLE) && RxBuf)  // Anything to do?
  {
    if( debug )
    {
      uint16_t l = formatCEC( (uint8_t *)RxBuf, RxLen );
      HAL_UART_Transmit( &huart2, cecFrame, l, 250 );
    }
    int16_t key = AnswerCEC();
    HandleKeys( key );

    /* We should be done with this buffer */
    DequeueBuffer();
  }
}

/*
 * All the callbacks needed to manage USB and CEC
 * Including SysTick
 */

void HAL_SYSTICK_Callback( void )
{
  uint16_t timeOut;
  
  // Declared in main.c
  extern void serveButton(void);
  
  serveButton();
  
  //Check for LED timeout
  if( ledOn && (HAL_GetTick() - ledTick > LED_TO) )
  {
    HAL_GPIO_WritePin( LED_GPIO, LED_PIN, GPIO_PIN_RESET );
    ledOn = 0;
  }

  //  Send reports at the stated idle interval or, when needed, at least 10ms apart
  
  // Tick counter
  static uint16_t count = 0;

  // Read idle interval
  uint16_t idle = USBD_HID_GetIdleInterval( &hUsbDeviceFS );
 
  count++;
  
  // USB report state machine
  switch (reportAction)
  {
  case REPORT_IDLE:
    if ((idle > 0) && (count >= idle))
    {
      // An idle timer is set and expired, report what we have
      USBD_HID_SendReport( &hUsbDeviceFS, HIDreport, 8 );
      // Reset counter
      count = 0;
    }
    break;
    
  case REPORT_PRESS:
    if (count >= idle)
    {
      // Idle timer expired or not there at all, send the report in any case!
      USBD_HID_SendReport(&hUsbDeviceFS, HIDreport, 8);
      count = 0;
      // Prepare to send key release
      HIDreport[2] = 0;
      reportAction = REPORT_RELEASE;      
    }
    break;
    
  case REPORT_RELEASE:
    // Make sure to wait a least 10ms between reports, even if no idle timer
    timeOut = (idle > 0) ?  idle : 10;
    if (count >= timeOut)
    {
      // Idle timer expired or not there at all, but need to send report
      USBD_HID_SendReport(&hUsbDeviceFS, HIDreport, 8);
      count = 0;
      // Back to idling
      reportAction = REPORT_IDLE;      
    }

  }
 
}

void HAL_CEC_RxCpltCallback(CEC_HandleTypeDef *hcec, uint32_t RxFrameSize)
{
  /* Reminder: hcec->RxXferSize is the sum of opcodes + operands
   * (0 to 14 operands max).
   * If only a header is received, hcec->RxXferSize = 0 */

  (void)hcec;
  /* Handle circular buffer */
  EnqueueBuffer(RxFrameSize);
}

void pulseLED( void )
{
  ledTick = HAL_GetTick();
  ledOn   = 1;
  HAL_GPIO_WritePin( LED_GPIO, LED_PIN, GPIO_PIN_SET );
}

