/**
  ******************************************************************************
  * File Name          : HDMI_CEC.c
  * Date               : 17/04/2015 21:53:02
  * Description        : This file provides code for the configuration
  *                      of the HDMI_CEC instances.
  ******************************************************************************
  *
  * COPYRIGHT(c) 2015 STMicroelectronics
  * COPYRIGHT(c) 2017 newbrain
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include "hdmi_cec.h"
#include "gpio.h"
#include "PortDefs.h"
#include "CECbuffer.h"

CEC_HandleTypeDef hcec;
uint16_t logicalAddress;

// List of possible logical addresses
static const uint8_t possibleAddresses[] =
{ 
  HDMI_CEC_PLAYBACK1,
  HDMI_CEC_PLAYBACK2,
  HDMI_CEC_PLAYBACK3
};

static volatile bool transmitOngoing = false;

/* HDMI_CEC init function */
void MX_HDMI_CEC_Init(void)
{

  hcec.Instance                       = CEC;
  hcec.Init.SignalFreeTime            = CEC_DEFAULT_SFT;
  hcec.Init.Tolerance                 = CEC_EXTENDED_TOLERANCE;
  hcec.Init.BRERxStop                 = CEC_RX_STOP_ON_BRE;
  hcec.Init.BREErrorBitGen            = CEC_BRE_ERRORBIT_GENERATION;
  hcec.Init.LBPEErrorBitGen           = CEC_LBPE_ERRORBIT_GENERATION;
  hcec.Init.BroadcastMsgNoErrorBitGen = CEC_BROADCASTERROR_NO_ERRORBIT_GENERATION;
  hcec.Init.SignalFreeTimeOption      = CEC_SFT_START_ON_TX_RX_END;
  hcec.Init.OwnAddress                = 1 << possibleAddresses[0];
  hcec.Init.ListenMode                = CEC_REDUCED_LISTENING_MODE;
  hcec.Init.RxBuffer                  = CecBuf;
  HAL_CEC_Init(&hcec);

}

void HAL_CEC_MspInit(CEC_HandleTypeDef* hcec)
{

  GPIO_InitTypeDef GPIO_InitStruct;
  if(hcec->Instance==CEC)
  {
    /* Peripheral clock enable */
    __CEC_CLK_ENABLE();
  
    /**HDMI_CEC GPIO Configuration    
    PA5     ------> CEC 
    */
    GPIO_InitStruct.Pin = CEC_PIN;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
    GPIO_InitStruct.Alternate = GPIO_AF1_CEC;
    HAL_GPIO_Init(CEC_GPIO, &GPIO_InitStruct);

    /* Peripheral interrupt init*/
    HAL_NVIC_SetPriority(CEC_CAN_IRQn, 9, 0);
    HAL_NVIC_EnableIRQ(CEC_CAN_IRQn);
  }
}

void HAL_CEC_MspDeInit(CEC_HandleTypeDef* hcec)
{

  if(hcec->Instance==CEC)
  {
    /* Peripheral clock disable */
    __CEC_CLK_DISABLE();
  
    /**HDMI_CEC GPIO Configuration    
    PB10     ------> CEC 
    */
    HAL_GPIO_DeInit( CEC_GPIO, CEC_PIN );

    /* Peripheral interrupt Deinit*/
    HAL_NVIC_DisableIRQ(CEC_CAN_IRQn);

  }
} 

HAL_StatusTypeDef HDMI_CEC_LogicalAddressAllocation(void)
{
  for (size_t i = 0; i < sizeof possibleAddresses; i++)
  {
    logicalAddress = possibleAddresses[i];
    hcec.Init.OwnAddress = (1 << logicalAddress); /* Own address = bitmask */
    // Disable the peripheral
    __HAL_CEC_DISABLE(&hcec);
    // Clear and set the new address
    HAL_CEC_SetDeviceAddress(&hcec, CEC_OWN_ADDRESS_NONE);
    HAL_CEC_SetDeviceAddress(&hcec, hcec.Init.OwnAddress);
    // Re-enable the peripheral
    __HAL_CEC_ENABLE(&hcec);
    
    transmitOngoing = true;
    HAL_CEC_Transmit_IT(&hcec, logicalAddress, logicalAddress, NULL, 0);
    while (transmitOngoing)
      ;

    // Check the error, Ack not expected -> TXACKE expected
    if ((HAL_CEC_GetError(&hcec) & HAL_CEC_ERROR_TXACKE) != 0)
    {
      /* No ACK, address is free, we can keep it! */
      return HAL_OK;
    }
  }
  return HAL_ERROR;
}

void HAL_CEC_TxCpltCallback(CEC_HandleTypeDef *hcec)
{
  (void)hcec;
  // Done transmitting
  transmitOngoing = false;
}

void HAL_CEC_ErrorCallback(CEC_HandleTypeDef *hcec)
{
  (void)hcec;
  // Transmission should have ended...
  transmitOngoing = false;
}


