/**
 ******************************************************************************
 * File Name          : main.c
 * Date               : 17/04/2015 21:53:03
 * Description        : Main program body
 ******************************************************************************
 *
 * COPYRIGHT(c) 2015 STMicroelectronics
 * COPYRIGHT(c) 2017 newbrain
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "CECconstants.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"
#include "usbd_kb_hid.h"
#include "hdmi_cec.h"
#include "CECtranslate.h"
#include "PortDefs.h"
#include <stdbool.h>

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config( void );

void ProgramDataOptByte();
void ShowAddress();

static uint8_t HIDreport[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };

// "usb kbd test\n"
static const uint8_t kbtest[] = {
  0x18,
  0x16,
  0x05,
  0x2C,           // "usb "
  0x0E,
  0x05,
  0x07,
  0x2C,           // "kbd "
  0x17,
  0x08,
  0x16,
  0x17,           // "test"
  0x2C,
  0x27,
  0x28                  // " 0\n"
};

static const uint8_t kbshft[] = {
  0x02,
  0x00,
  0x00,
  0x00,           // "Usb "
  0x20,
  0x00,
  0x00,
  0x00,           // "Kbd "
  0x00,
  0x00,
  0x00,
  0x00,           // "test"
  0x00,
  0x00,
  0x00                  // " 0\n"
};

void KbdReport( void )
{
  static uint8_t kCount = 0x1E;
  HAL_Delay( 5000 );
  for( uint16_t i = 0; i < 15; i++ )
  {
    HIDreport[0] = kbshft[i];
    HIDreport[2] = (i == 13) ? kCount : kbtest[i];
    HAL_GPIO_TogglePin( LED_GPIO, LED_PIN );
    USBD_HID_SendReport( &hUsbDeviceFS, HIDreport, 8 );
    HIDreport[2] = 0;
    HIDreport[0] = 0;
    HAL_Delay(10);
    USBD_HID_SendReport( &hUsbDeviceFS, HIDreport, 8 );
    HAL_GPIO_TogglePin( LED_GPIO, LED_PIN );
  }
  if( kCount++ >= 0x27 )
    kCount = 0x1E;
}

static volatile enum {
  CEC2USB, HIDTEST, BLINKY, PROGRAM
} task = CEC2USB;



/* Deafult physical address if not found in flash option byte */
static uint8_t physHi = 0x30;


/* State Machine handling of button for flash programming */
#define CLICKDEBOUNCE 40    /* 40ms for debouncing   */
#define CLICKLONG     3000  /* 3s for long click     */
#define CLICKSHORT    100   /* 100ms for short click */


typedef enum
{
  Idle = 0,
  Debouncing,
  Pressed,
  Waiting
} BtnState;

typedef enum
{
  NoPress   = 0,
  LongPress,
  ShortPress
} KeyPress;


static volatile KeyPress key = NoPress;

void serveButton( void )
{
  static BtnState btnState = Idle;
  static uint32_t btnTick;
  
  uint32_t now = HAL_GetTick();
  bool     btn = (GPIO_PIN_RESET == HAL_GPIO_ReadPin(KEY_GPIO, KEY_PIN));
  
  switch (btnState)
  {
  case Idle:
    if (btn)
    {
      /* The button has been depressed */
      /* Remember when */
      btnTick  = now;
      /* and move to Debouncing state */
      btnState = Debouncing;
    }
    break;
      
  case Debouncing:
    if (btn)
    {
      if (now - btnTick > CLICKDEBOUNCE)
        /* Real press */
        btnState = Pressed;
    }
    else
    {
      /* It was a bounce, start over */
      btnState = Idle;
    }
    break;
      
  case Pressed:
    if (btn)
    { /* Still pressed, is this a long press? */
      if (now - btnTick > CLICKLONG)
      {
        /* A long press has been detected */
        key = LongPress;
        /* Wait for release */
        btnState = Waiting;
      }
    }
    else
    { /* Released, is it a short press? */
      if (now - btnTick > CLICKSHORT)
      {
        /* A short press has been detected */
        key = ShortPress;
      }
      /* Return to idle */
      btnState = Idle;
    }
    break;
    
  case Waiting:
    if (!btn)
    {
      btnState = Idle;
    }
    break;
  }
}


/* Reads and resets the button */
static inline KeyPress ReadBtn(void)
{
  KeyPress tmpKey = key;
  key = NoPress;
  return tmpKey;
}

static uint8_t rxb[2] = "\r\n";

#define CTL_B ('B'&0x1F)
#define CTL_C ('C'&0x1F)
#define CTL_D ('D'&0x1F)
#define CTL_H ('H'&0x1F)
#define CTL_R ('R'&0x1F)

void HAL_UART_RxCpltCallback( UART_HandleTypeDef *huart )
{
  uint8_t *p = rxb;
  uint16_t l = 1;

  if( debug ) { // Accept some keys
    switch( *rxb ) {
    case CTL_B:
      task = BLINKY;
      *rxb = 'B';
      break;
    case CTL_C:
      task = CEC2USB;
      *rxb = 'C';
      break;
    case CTL_H:
      task = HIDTEST;
      *rxb = 'H';
      break;
    case CTL_D:
      debug = 0;
      *rxb = 'd';
      break;
    case CTL_R:
      NVIC_SystemReset();
      break; // ahahahah!
    case '\r':
      l = 2;
      break;
    default:
      break;
    }
    HAL_UART_Transmit( huart, p, l, 500 );
  }
  else { // Only debug key accepted
    if( *rxb == CTL_D )
    {
      debug = 1;
      *rxb = 'D';
      HAL_UART_Transmit( huart, p, l, 500 );
    }
  }

  // But wait for the next key
  HAL_UART_Receive_IT( huart, rxb, 1 );
}


int main( void )
{

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_HDMI_CEC_Init();
  MX_USART2_UART_Init();
  MX_USB_DEVICE_Init();

  // Start Usart rx
  HAL_UART_Receive_IT( &huart2, rxb, 1 );

  // Read Physical Address from Option Byte Data1
  uint8_t pH = HAL_FLASHEx_OBGetUserData(OB_DATA_ADDRESS_DATA1);
  // If not programmed, keep default value, if programmed, store it
  if( pH <= 0x50 )
    physHi = pH;
  /* CEC transfer initialization */
  CecInitialExchange( physHi );

  /* Almost Infinite loop */
  while( task != PROGRAM )
  {
    switch( task ) {

    case CEC2USB:       // CEC Handling
    default:            // should never happen...
      Translate();
      break;

    case HIDTEST:       // USB HID keyboard test
      KbdReport();
      break;

    case BLINKY:        // Blinky
      for( uint16_t i = 0; i < 10; i++ )
      {
        HAL_GPIO_TogglePin( LED_GPIO, LED_PIN );
        HAL_Delay( 30 );
      }
      HAL_Delay( 1700 );
      break;
    }
    if (ReadBtn() == LongPress) task = PROGRAM;
  }
  // We are here to program the option bytes.
  // After that, a reset is enforced, so out of the main loop
  for (;;)
  {
    switch( ReadBtn() )
    {
    case LongPress:
      ProgramDataOptByte();
      break; // Trust me...
    case ShortPress:
      // incrementing address
      physHi += 0x10;
      if (physHi > 0x50) physHi = 0x10; // At most, 5 HDMI considered
      // Fall-trough intentional.
    default:
      ShowAddress();
      break;
    }
  }
}


// ShowAddress: blinks the currently set physical address
void ShowAddress( void )
{
  for( uint16_t i = 0; i < (physHi >> 4); i++ )
  {
    HAL_GPIO_WritePin( LED_GPIO, LED_PIN, GPIO_PIN_SET );
    HAL_Delay( 250 );
    HAL_GPIO_WritePin( LED_GPIO, LED_PIN, GPIO_PIN_RESET );
    HAL_Delay( 250 );
  }
  HAL_Delay( 2000 );
}

void ProgramDataOptByte( void )
{
  FLASH_OBProgramInitTypeDef optionBytes;

  // Read OPtion Bytes (but not Data0 and 1...)
  HAL_FLASHEx_OBGetConfig( &optionBytes );

  // Prepare structure to reprogram OB
  optionBytes.DATAData    = physHi;
  optionBytes.DATAAddress = OB_DATA_ADDRESS_DATA1;
  optionBytes.OptionType |= OPTIONBYTE_DATA;
  // RDP is restored by the erase function call
  optionBytes.OptionType &= ~OPTIONBYTE_RDP;

  // Unlock the Option Bytes
  HAL_FLASH_Unlock();
  HAL_FLASH_OB_Unlock();

  // Erase Option Bytes is mandatory
  HAL_FLASHEx_OBErase();

  // Reprogram option bytes
  HAL_FLASHEx_OBProgram( &optionBytes );

  if( debug )
  {
    static uint8_t sPhys[14] = "Ph Addr 0000\r\n";
    sPhys[8] = toHex( (physHi >> 4) );
    HAL_UART_Transmit( &huart2, sPhys, 14, 500 );
  }

  // Force re-reading (and reset)
  HAL_FLASH_OB_Launch();
  // Wait for reset
  for( ;; )
    ;
}


/** System Clock Configuration
 */
void SystemClock_Config( void )
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

  RCC_OscInitStruct.OscillatorType =
    RCC_OSCILLATORTYPE_HSI | RCC_OSCILLATORTYPE_HSI48 |
    RCC_OSCILLATORTYPE_HSE | RCC_OSCILLATORTYPE_LSE | RCC_OSCILLATORTYPE_HSI14;
  RCC_OscInitStruct.LSEState = RCC_LSE_OFF;
  RCC_OscInitStruct.HSEState = RCC_HSE_OFF;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_OFF;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_OFF;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL12;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV2;
  HAL_RCC_OscConfig( &RCC_OscInitStruct );

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_SYSCLK;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI48;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  HAL_RCC_ClockConfig( &RCC_ClkInitStruct, FLASH_LATENCY_1 );

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB | RCC_PERIPHCLK_USART1
      | RCC_PERIPHCLK_CEC;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_SYSCLK;
  PeriphClkInit.CecClockSelection = RCC_CECCLKSOURCE_HSI;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;
  HAL_RCCEx_PeriphCLKConfig( &PeriphClkInit );

  static RCC_CRSInitTypeDef CRSinit;
  __CRS_CLK_ENABLE();
  
  CRSinit.Prescaler             = RCC_CRS_SYNC_DIV1;
  CRSinit.Source                = RCC_CRS_SYNC_SOURCE_USB;
  CRSinit.ReloadValue           = __HAL_RCC_CRS_CALCULATE_RELOADVALUE( 48000000, 1000 );
  CRSinit.ErrorLimitValue       = RCC_CRS_ERRORLIMIT_DEFAULT;
  CRSinit.HSI48CalibrationValue = RCC_CRS_HSI48CALIBRATION_DEFAULT;
  HAL_RCCEx_CRSConfig( &CRSinit );
    
    __SYSCFG_CLK_ENABLE();
}


#ifdef USE_FULL_ASSERT

/**
 * @brief Reports the name of the source file and the source line number
 * where the assert_param error has occurred.
 * @param file: pointer to the source file name
 * @param line: assert_param error line source number
 * @retval None
 */
void assert_failed( uint8_t* file, uint32_t line )
{
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
}

#endif

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
